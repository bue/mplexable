# Mplexable Data Extraction Tutorial


## Mplexable

Install mplexable as described in the [HowTo - Installation Mplexable](https://gitlab.com/bue/mplexable/-/blob/master/README.md#howto-installation-mplexable) section in the `README.md` file


## Dataset

The demo dataset is stored in [synapse](https://www.synapse.org/).
These are only 9 cyclic IF rounds from 3 TMA cores (more details is in the `README.txt` file that ships with the download).
Anyhow, this dataset is ~28[GB]!

To download the data set,
you have first to [register at synapse](https://www.synapse.org/#!RegisterAccount:0), to get a user account,
and you have to install the synapse client.
```bash
conda activate mplexable
pip install synapseclient
```

Then download the dataset from the command line.
```bash
synapse get -r syn26958265
```

The dataset is structured as the following:
```
cmIF_2021-01-07_demo
  +
  +-- CziImages  # zeiss image files, for metadata extraction.
  +     +
  +     +-- HER2B-K185
  +     +     +
  +     +     +-- original  # round 1 czi file for microscopy scene position and exposure time metadata.
  +     +     +-- splitscenes  # one czi file per TMA core (scene) and round for exposure time metadata.
  +     +
  +     +-- JE-TMA-66
  +           +
  +           +-- original  # round 1 czi file for microscopy scene position and exposure time metadata.
  +           +-- splitscenes  # one czi file per TMA core (scene) and round for exposure time metadata.
  +
  +-- RawImages  # raw tiff files, generated from czi files using zeiss zen software.
  +     +
  +     +-- HER2B-K185  # one tiff file per TMA core (scene), round, and channel.
  +     +-- JE-TMA-66  # one tiff file per TMA core (scene), round and channel.
  +
  +-- RegisteredImages.matlab  # registered tiff images, resulting from matlab based registartion code.
  +     +                        these are provided because matlab is licensed, proprietary software.
  +     +
  +     +-- HER2B-K185_sceneA01  # one tiff file per TMA core (scene), round and channel.
  +     +-- JE-TMA-66_sceneB01  # one tiff file per TMA core (scene), round and channel.
  +     +-- JE-TMA-66_sceneE01  # one tiff file per TMA core (scene), round and channel.
  +
  +-- README.txt  # read me file with some information about samples and a file listing.
  +-- 20210107_demo_solution_mpleximage_data_extraction_pipeline.ipynb  # solved jupyter notebook,
              to look up how it's done, if you get stuck in the tutorial.
```
The `SYNAPSE_METADATA_MANIFEST.tsv` files can be ignored.


## Jupyter Notebook

Download the mplexable data extraction notebook template (raw version),
put it into the `cmIF_2021-01-07_demo` dataset folder,
and rename it appropriately.

Unix:
```bash
cd cmIF_2021-01-07_demo
wget https://gitlab.com/bue/mplexable/-/raw/master/jupyter/mpleximage_data_extraction_pipeline.ipynb
mv mpleximage_data_extraction_pipeline.ipynb 20210107_demo_mpleximage_data_extraction_pipeline.ipynb
```

Windows:
```dos
This is the URL: https://gitlab.com/bue/mplexable/-/raw/master/jupyter/mpleximage_data_extraction_pipeline.ipynb
Use your mouse and keyboard.
```

## Biomarkers

All DAPI, blank, and protein marker used in the batch have to be specified.
Actually, all biomarkers from this demo dataset are already specified in the mplexable default `config.py` file.
Anyhow, please have a look at the [HowTo - Protei Marker](https://gitlab.com/bue/mplexable/-/blob/master/README.md#howto-protein-marker) section in the `README.md` file,
and your local `~/.mplexable/config.py` file!


## Data Extraction

The notebook is quite descriptive.
Simply follow its instruction.
If you get stuck, have a look at the solution notebook that came packed with the dataset.


**Basic pipeline elements**

+ `util.dchange_fname`: this is just a utility to change filenames efficiently.

+ **1.1**, **1.3**: `sane.count_images` and `sane.check_markers` will help you to check your files for completeness,
    check if the filenames are in agreement with the naming convention,
    and check if the protein labels stated in the  filename are already specified in the local config.py file.
    This step is important because for mplexable the required experimental metadata is stored in these filenames.

+ **1.4**: `sane.visualize_raw_images_spaw` will generate a QC plots for each microscopy scene, containing all rounds.
    These plots are valuable to find even the trickiest filename errors and check the raw data for completeness.

+ **2.0**: `imgmeta.fetch_meta_batch` will extract image metadata stored in the czi files.

+ **2.1**: `util.tma_grid`: is optional, but can be used to layout TMA cores in a grid structure,
    to map each TMA core to a well plate like coordinate like A01.

+ **3.1**, **3.2**, **3.3**, **3.4**: `util.template_dddcrop` and `util.gridcrop` are utilities to help you to specify the `ddd_crop` dictionary.
    It is a lot of work to specify the crop coordinates.
    `regist.save_cropcoor` is there to back up this work in a json file, just in case.

+ **4.0**: `regist.regist_spawn`: will run image registration and do the cropping and stores the result in the RegisteredImages folder.
    Unfortunately, the best registration code we have and use is written in matlab, which is licensed, proprietary software.
    We have not been able to achieve similar good results with our python adaption.
    For this tutorial, tiffs registered with this matlab code are provided with the dataset.
    If you want to use the provided registered images, please rename the RegisteredImages.matlab folder to RegisteredImages.

+ **4.1**: `regist.visualize_reg_images_spawn` will generate QC plots for each cropped scene, containing all rounds.
    These images are indispensable to spot registration errors.
    From this demo data set, all cores will register.
    In a real-world case, if images not register, we maker a deprecated folder inside the RegisteredImages folder, and move the scenes into it that we are unable to register.

+ **5.0**: `imgmeta.exposure_matrix`: will generate an exposure time csv and png plot.
    This csv is handy to spot exposure time settings error.
    To find errors, in the csv, check the last column (exposure_mean) and the bottom row (exposure_sum) of the matrix.
    For any marker, all slide and scenes from a batch should have the same exposure time settings!

+ **5.1**, **5.2**, **5.3**, **5.4**: `util.template_dddetc` is a utility to help to generate the `ddd_etc` exposure time correction dictionary.
    `regist.save_exposuretimecorrect` is there to back up this work in a json file, just in case.

+ **6.0**: `basic.marker_table` will generate a marker table, one row per round, one column per microscopy channel.
    The marker table is printed on screen and also stored in a tsv file.
    This table comes in handy, to look up the quenching rounds (for afsub.afsub_spawn) and the last DAPI round (for feat.filter_features_spawn).

+ **7.0**: `afsub.afsub_spawn` will generate registered autofluorescence subtracted images, which are stored in the SubtractedRegisteredImages folder.

+ **7.1**: `regist.visualize_reg_images_spawn` will for the specified microscopy channels generate QC plots for each af subtracted registered scene, containing all rounds.

+ **8.0**: `segment.segment_spawn` will be used for nucleus and cell segmentation, and matching the nucleus and cell segmentation labels.

+ **9.0**: `thresh.auto_thresh_spawn` will auto threshold every marker with li, otsu, and the triangle algorithm.
    In our experience, auto threshold works only proper for DAPI.
    Li auto threshold DAPI values will be used in the feat.filter_features_spawn step.

+ **10.0**: `feat.extract_features_spawn` will extract for every marker all possible cell partition and store the values in the features raw csv file.

+ **10.1**: `feat.filter_features_spawn` will filter and patch the feature raw csv values to the relevant partitions,
    utilizing therefore the biomarker information specified in the local config.py file.
    The result is stored in the features patched csv file.

+ **10.2**: `feat.feature_correct_labels_spawn` will generate an additional cell label file with matched cell and nucleus information,
    and nucleus expanded for cell types we had no cell segmentation markers in the panel.
    This cell label file is not directly suited for feature extraction,
    but can be used for downstream analysis, for example in [napari](https://napari.org/) because it incorporates some filtering and patching steps from above.

+ **11.0**: `segment.nuccell_zprojlabel_imgs_spawn` will generate a QC multi plot, one for each slide_scene.
  The QC multi plot contains DAPI and cell segmentation marker z projection plots, nucleus and cell label plots, and tissue edge detection plots.

+ **12.0**: `util.sweep` will sweep the processing directory so that everything is nice and tidy again.
      Thereby, all scripts generated during processing will be moved to a scripts folder.


**Optional pipeline elements**

+ `ometiff.ometiff_spawn` will generate non-pyramide multichannel [ome-tiff](https://docs.openmicroscopy.org/ome-files-cpp/0.5.0/ome-model/manual/html/index.html) files.
Multichannel ome-tiff files became the de facto standard in the multiplex imaging community.
    Unfortunately, the python based [aicsimageio](https://github.com/AllenCellModeling/aicsimageio) library can not yet generated ome-tiff in pyramide format,
    but this might change in the near future.

+ `util.compress_xz_spawn` will do [xz](https://en.wikipedia.org/wiki/XZ_Utils) level 9 compression, on czi or raw tiffs to save disk space.

+ `util.decompress_xz_spawn` will unzip xz compressed czi or raw tiff files.


That's all, Folks!
